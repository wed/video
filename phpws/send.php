#!/php -q
<?php

// Run from command prompt > php demo.php
require_once("websocket.server.php");
require_once("websocket.client.php");
require_once("config.php");

/**
 * Demo socket server. Implements the basic eventlisteners and attaches a resource handler for /echo/ urls.
 *
 *
 * @author Chris
 *
 */
class DemoSocketServer implements IWebSocketServerObserver{
	protected $debug = true;
	protected $server;

	public function __construct(){
		global $send_server;
		$this->server = new WebSocketServer("tcp://" . $send_server, 'superdupersecretkey');
		$this->server->addObserver($this);
	}

	public function onConnect(IWebSocketConnection $user){
		$this->say("[DEMO] {$user->getId()} connected");
	}

	public function onMessage(IWebSocketConnection $user, IWebSocketMessage $msg){
		global $recive_server;
        $msg = WebSocketMessage::create($msg->getData());

        $client = new WebSocket("ws://" . $recive_server);
        $client->open();
        $client->sendMessage($msg);
        $client->close();
	}

	public function onDisconnect(IWebSocketConnection $user){
		$this->say("[DEMO] {$user->getId()} disconnected");
	}

	public function onAdminMessage(IWebSocketConnection $user, IWebSocketMessage $msg){
		$this->say("[DEMO] Admin Message received!");

		$frame = WebSocketFrame::create(WebSocketOpcode::PongFrame);
		$user->sendFrame($frame);
	}

	public function say($msg){
		echo "$msg \r\n";
	}

	public function run(){
		$this->server->run();
	}
}

// Start server
$server = new DemoSocketServer();
$server->run();